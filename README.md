<div align="center">
  <a href="https://github.com/webpack/webpack">
    <img width="200" height="200" src="https://webpack.js.org/assets/icon-square-big.svg">
  </a>
</div>

[![npm][npm]][npm-url]

# @evolves/php-loader

PHP loader for webpack 5

## Getting Started

To begin, you'll need to install `@evolves/php-loader`:

```console
$ npm install @evolves/php-loader --save-dev
```

Then add the loader to your `webpack` config. For example:

**webpack.config.js**

```js
module.exports = {
  module: {
    rules: [
      {
        test: /.ext$/,
        use: [
          {
            loader: `@evolves/php-loader`,
            options: {
              name: "[name].html",
              outputPath: "/"
            },
          },
        ],
      },
    ],
  },
};
```

## Contributing

Please take a moment to read our contributing guidelines if you haven't yet done so.

[CONTRIBUTING](./CONTRIBUTING.md)

## License

[MIT](./LICENSE.md)

[npm]: https://img.shields.io/npm/v/@evolves/php-loader.svg
[npm-url]: https://npmjs.com/package/@evolves/php-loader
