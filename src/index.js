const util = require('util');
const exec = util.promisify(require('child_process').exec);

import path from 'path';
import loaderUtils from 'loader-utils';
import {validate} from 'schema-utils';
import schema from './options.json';

export default function loader(content, map, meta) {
  const options = loaderUtils.getOptions(this);

  validate(schema, options, {
    name: 'PHP Loader',
    baseDataPath: 'options',
  });

  const context = options.context || this.rootContext;
  const name = options.name || '[contenthash].[ext]';

  const url = loaderUtils.interpolateName(this, name, {context, content, regExp: options.regExp,});
  let outputPath = url;
  if (options.outputPath) {
    if (typeof options.outputPath === 'function') {
      outputPath = options.outputPath(url, this.resourcePath, context);
    } else {
      outputPath = path.posix.join(options.outputPath, url);
    }
  }

  let self = this;
  let callback = this.async();

  async function runPhp() {
    try {
      let {stdout, stderr} = await exec(`php ${self.resourcePath}`);

      if (stderr) {
        self.emitError(stderr);
        callback(stderr);
      } else {
        self.emitFile(outputPath, stdout, null);
        callback(null, JSON.stringify(stdout), map, meta);
      }
    } catch (e) {
      callback(e);
    }
  }

  runPhp();
}
